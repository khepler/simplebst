# README #

To build the demo, just compile the crazy_trees.cpp driver file.

### Why?! ###

This project demonstrates a couple things in C++, mostly templates, polymorphism.  I needed to construct a binary search tree in an attempt to understand [an Amazon interview question](http://careercup.appspot.com/question?id=5673062741573632).  Then I asked myself if a class could replace (a reference to) itself.  This kind of thing happens when I can't sleep.  Sorry.

### Who do I blame for this travesty? ###

* All blame resides with Kris Hepler
