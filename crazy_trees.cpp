#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

#include "node_fun.h"

#define ARRAY_LEN 15

typedef unsigned int value_t;

int main( int argc, char** argv ) {
	value_t a[ ARRAY_LEN ] = { 8, 4, 2, 1, 3, 6, 5, 7, 12, 10, 9, 11, 14, 13, 15 };
	size_t aLen = ARRAY_LEN;

	// load test data
	node_branch<value_t> root( a[ 0 ] ); 
	for( size_t i = 1; i < aLen; i++ )
		root.insert( a[ i ] );

	// output tree
	cout << root << endl;

	node_t<char> *chRoot = new node_stub<char>( chRoot );
	srand( time( NULL ) );
	for( size_t i = 0; i < 32; i++ )
		chRoot->insert( 65 + (double)rand() / RAND_MAX * 26 );
	cout << *chRoot << endl;
	

	return 0;
}
