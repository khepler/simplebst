#ifndef NODE_T
#define NODE_T

#include <iostream>
using namespace std;

template <class value_t>
struct node_t {
	virtual void insert( const value_t &v ) = 0;
	virtual const node_t<value_t>* get( const value_t &v ) const = 0;
	virtual ostream& print( ostream &oStr ) const = 0;
};

template <class value_t>
struct node_branch;

template <class value_t>
struct node_stub : public node_t<value_t> {
	node_t<value_t> **ptr;
	node_stub( node_t<value_t>* &p ) { ptr = &p; };
	const node_t<value_t>* get( const value_t &v ) const { return NULL; };
	void insert( const value_t &v ) {
		node_branch<value_t> *node = new node_branch<value_t>( v, this );
		cout << "Allocated new node: " << *node << endl;
		*ptr = node;
		ptr = &(node->left);
	};
	ostream& print( ostream& oStr ) const { return oStr << '-'; };
};

template <class value_t>
struct node_branch : public node_t<value_t> {
	node_t<value_t> *left, *right;
	value_t val;
	node_branch( const value_t &v ) {
		left = new node_stub<value_t>( left );
		right = new node_stub<value_t>( right );
		val = v;
	}
	node_branch( const value_t &v, node_t<value_t>* left_alloc ) {
		left = left_alloc;
		right = new node_stub<value_t>( right );
		val = v;
	};
	~node_branch( void ) { delete left, right; };
	void insert( const value_t &v ) {
		if( v < val ) {
			cout << v << " < " << val << endl;
			left->insert( v );
		} else if( v > val ) {
			cout << v << " > " << val << endl;
			right->insert( v );
		} else
			cout << "Already have " << v << endl;
	};
	const node_t<value_t>* get( const value_t &v ) const {
		if( v < val ) return left->get( v );
		if( v > val ) return right->get( v );
		return this;
	};
	ostream& print( ostream &oStr ) const {
		return oStr << '<' << *left << ',' << val << ',' << *right << '>';
	};
};

// ostream glue
template <class value_t>
ostream& operator<<( ostream &oStr, const node_t<value_t> &node ) {
	return node.print( oStr );
}

#endif // NODE_T

